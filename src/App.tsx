import AuthInputs from "./components/AuthInputs.tsx";
import Header from "./components/Header.tsx";

function App() {
    return (
        <>
            <Header />
            <main>
                <AuthInputs />
            </main>
        </>
    );
}

export default App
