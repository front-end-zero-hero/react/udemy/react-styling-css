import React, {HTMLAttributes} from "react";

interface ButtonProps extends HTMLAttributes<HTMLButtonElement> {
    children: string;
}

const Button: React.FC<ButtonProps> = ({children, ...props}) => {
    return (
        <>
            <button type="button"
                    className="px-4 py-2 font-semibold uppercase rounded text-stone-900 bg-amber-400 hover:bg-amber-500" {...props}>{children}</button>
        </>
    )
}
export default Button;
